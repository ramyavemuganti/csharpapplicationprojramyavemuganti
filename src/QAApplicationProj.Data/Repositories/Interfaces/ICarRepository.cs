﻿using QAApplicationProj.Data.Entities;

namespace QAApplicationProj.Data.Repositories.Interfaces 
{
    public interface ICarRepository 
    {
        bool Exists(Car car);

        void Create(Car car);
    }
}

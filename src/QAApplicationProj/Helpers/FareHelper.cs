﻿using System;

namespace QAApplicationProj.Helpers
{
    public static class FareHelper
    {
        /// <summary>
        /// A helper method that adds the fare amounts between two trips and returns a result.
        /// This method throws an OverflowException when the sum of the fares is larger than the biggest decimal value supported by .NET.
        /// </summary>
        /// <param name="firstFare"></param>
        /// <param name="secondFare"></param>
        /// <returns></returns>
        public static decimal AddFares(decimal firstFare, decimal secondFare) {

            if (firstFare < 0) {
                throw new ArgumentOutOfRangeException(nameof(firstFare), "The first fare must be positive.");
            }

            if (secondFare < 0) {
                throw new ArgumentOutOfRangeException(nameof(secondFare), "The second fare must be positive.");
            }

            return firstFare + secondFare;
        }
    }
}

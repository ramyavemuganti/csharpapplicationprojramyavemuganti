﻿using Microsoft.AspNetCore.Mvc;
using QAApplicationProj.Data.Repositories.Interfaces;
using QAApplicationProj.Helpers;
using QAApplicationProj.Mappers;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;

namespace QAApplicationProj.Controllers
{
    [Route("api/[controller]")]
    public class CarController : Controller
    {
        private readonly ICarRepository _carRepository;

        public CarController(ICarRepository carRepository) {
            _carRepository = carRepository;
        }

        /// <summary>
        /// Attempts to call CarRepository to create a Car in the "database" after said Car passes validation.
        /// </summary>
        /// <param name="request">The Car to create.</param>
        /// <returns>
        /// Returns a CreateDriverResponse with wasSuccessful = true, except when validation fails or an exception is encountered.
        /// In the latter cases, the Message proprety is set on the response.
        /// </returns>
        [HttpPost]
        public CreateCarResponse CreateCar(CarRequest request)
        {
            var response = new CreateCarResponse();
            if (!CarHelper.IsRequestValid(request)) {
                response.Message = "The request did not pass validation.";
                return response;
            }

            var carEntity = CarMapper.Map(request);

            try 
            {
                if (_carRepository.Exists(carEntity)) {
                    response.Message = "The requested Car already exists.";
                    return response;
                }

                _carRepository.Create(carEntity);
                response.WasSuccessful = true;
            } 
            catch
            {
                response.Message = "An unhandled exception occurred while creating the requested Car.";
                return response;
            }
            
            return response;
        }
    }
}
